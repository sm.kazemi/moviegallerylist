package com.smr.rubikatest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.smr.rubikatest.common.horizontalLinearLayoutManager
import com.smr.rubikatest.common.onLoadMore
import com.smr.rubikatest.common.showToast
import com.smr.rubikatest.databinding.ActivityMainBinding
import com.smr.rubikatest.list.VideosAdapter

class MainActivity : AppCompatActivity() {
    private lateinit var videoAdapter: VideosAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initRecycler(binding, generateFakeImageMovieUrl())
    }

    /**
     * list size must be multiply by 3
     * if your list items not enough fill it with null
     * e.g listOf("1","2","3","4",null,null)
     * **/
    private fun generateFakeImageMovieUrl(): List<String?> {
        // 18 item
        return listOf("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "")
    }

    private fun initRecycler(binding: ActivityMainBinding, movies: List<String?>) {

        videoAdapter = VideosAdapter { row, column ->
            showToast(getString(R.string.itemPositionDescription, column, row))
        }

        binding.rec.apply {
            horizontalLinearLayoutManager(context)
            adapter = videoAdapter
            onLoadMore {
                videoAdapter.showLoading()
            }
        }

        videoAdapter.addItems(movies)

    }
}