package com.smr.rubikatest.common

import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var isLoading: Boolean = false
    val loading = "-1"
    private var list: ArrayList<String?> = ArrayList()

    override fun getItemCount(): Int = list.size / 3

    fun getListSize() = list.size

    fun getItem(position: Int) = list[position]

    fun addItems(list: List<String?>) {

        hideLoading()
        this.list.apply {
            clear()
            addAll(list)
            notifyDataSetChanged()
        }
    }

    fun showLoading() {
        if (!isLoading) {
            list.add(loading)
            notifyItemInserted(6)
            isLoading = true
            "loading".logD("")
        }
    }

    private fun hideLoading() {
        if (isLoading) {
            list.removeAt(list.size - 1)
            notifyItemRemoved(list.size)
        }
        isLoading = false
    }


}