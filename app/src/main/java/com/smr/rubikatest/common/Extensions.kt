package com.smr.rubikatest.common

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.smr.rubikatest.R

fun RecyclerView.horizontalLinearLayoutManager(context: Context) {
    this.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)



}

fun RecyclerView.onLoadMore(operation: () -> Unit) {

    addOnScrollListener(object : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

            val currentItems = recyclerView.layoutManager!!.childCount
            val totalItems = recyclerView.layoutManager!!.itemCount

            val scrollOutItems =
                (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

            val positionInScroll = currentItems + scrollOutItems
            if ((positionInScroll == totalItems)) {
                operation()
            }

        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {

        }
    })
}

fun String.logD(flag : String){
    Log.d("TAg","$flag -> $this")
}

fun View.gone(){
    visibility = View.GONE
}

fun ImageView.loadUrl(url: String) {
    load(url) {
        crossfade(true)
        placeholder(R.drawable.ic_baseline_image_24)
        error(R.drawable.ic_baseline_broken_image_24)
    }
}

fun Activity.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Activity.showToast(message: Int) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}