package com.smr.rubikatest.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smr.rubikatest.common.BaseAdapter
import com.smr.rubikatest.common.logD
import com.smr.rubikatest.databinding.ItemLoadingBinding
import com.smr.rubikatest.databinding.ItemMovieBinding

class VideosAdapter(
    val onItemClicked: (row: String, column: Int) -> Unit
) : BaseAdapter(), MoviesCallback {
    private val videoType = 1
    private val loadingType = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            loadingType -> {
                LoadingViewHolder(
                    ItemLoadingBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            else -> {
                MovieViewHolder(
                    ItemMovieBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            loadingType -> {
                "loading type".logD("")
            }
            videoType -> {
                var itemIndex = position * 3
                (holder as MovieViewHolder)
                    .bind(
                        getItem(itemIndex),
                        getItem(++itemIndex),
                        getItem(++itemIndex),
                        callBack = this,
                        position = position
                    )
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position) == loading) {
            loadingType
        } else {
            videoType
        }
    }

    override fun onTopImageClicked(position: Int) {
        onItemClicked("اول", position + 1)
    }

    override fun onMiddleImageClicked(position: Int) {
        onItemClicked("دوم", position + 1)
    }

    override fun onBottomImageClicked(position: Int) {
        onItemClicked("سوم", position + 1)
    }

}

interface MoviesCallback {
    fun onTopImageClicked(position: Int)
    fun onMiddleImageClicked(position: Int)
    fun onBottomImageClicked(position: Int)
}