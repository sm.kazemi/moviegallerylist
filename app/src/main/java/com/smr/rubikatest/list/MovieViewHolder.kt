package com.smr.rubikatest.list

import androidx.recyclerview.widget.RecyclerView
import com.smr.rubikatest.common.gone
import com.smr.rubikatest.common.loadUrl
import com.smr.rubikatest.databinding.ItemMovieBinding

class MovieViewHolder(private val binding: ItemMovieBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(vararg imagesUrl: String?, callBack: MoviesCallback, position: Int) {
        imagesUrl[0]?.let { binding.imgTop.loadUrl(it) } ?: kotlin.run { binding.imgTop.gone() }
        imagesUrl[1]?.let { binding.imgMiddle.loadUrl(it) } ?: kotlin.run { binding.imgMiddle.gone() }
        imagesUrl[2]?.let { binding.imgBottom.loadUrl(it) } ?: kotlin.run { binding.imgBottom.gone() }

        binding.imgTop.setOnClickListener {
            callBack.onTopImageClicked(position)
        }

        binding.imgMiddle.setOnClickListener {
            callBack.onMiddleImageClicked(position)
        }

        binding.imgBottom.setOnClickListener {
            callBack.onBottomImageClicked(position)
        }

    }

}